package main

import (
	"fmt"
	"net/http"
	"math/rand"
	"time"
)

var messages = []string{
	"Wherever you go, there you are",
	"Live like you're going to die tomorrow. Learn like you will live forever.",
	"Your mother was a hamster and your father smelt of elderberries.",
	"42",
}

var bytesSent int = 0
var numRequests int = 0
var requests [4]int

// Say something witty
func handler(w http.ResponseWriter, r *http.Request) {
	rand.Seed(time.Now().Unix())

	index := rand.Intn(len(messages))

	// Why are we tracking these separately? That's a very good point.
	numRequests += 1
	requests[index] += 1

	bytes, _ := fmt.Fprintf(w,  messages[index])

	bytesSent += bytes

}

// Return statistics
func statsHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<html><head><title>The Uberlab's Prometheus Exporters Tutorial Super Cool Awesome Web Application.</title><body>")

	for index := range requests {
		fmt.Fprintf(w, "<p>Message: %s Responses: %d</p>", messages[index], requests[index])
	}

	fmt.Fprintf(w, "<p>Total Requests: %d</p>", numRequests)
	fmt.Fprintf(w, "<p>Total Bytes: %d</p>", bytesSent)

	fmt.Print("</body></html>")
}

func main() {
	// I think I need a makemap here.
	//requests := [len(messages)]int


	http.HandleFunc("/", handler)
	http.HandleFunc("/stats", statsHandler)
	http.ListenAndServe(":8080", nil)

	//for index := range requests {
	//	requests[index] = 0
	//}

}
